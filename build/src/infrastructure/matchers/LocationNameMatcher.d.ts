declare type LocationNameMatchResult = {
    isMatch: boolean;
};
export default class LocationNameMatcher {
    findMatch(aSearchedString: string, aNeedleString: string): LocationNameMatchResult;
}
export {};
