"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class LocationNameMatcher {
    findMatch(aSearchedString, aNeedleString) {
        let matchResult = {
            isMatch: false
        };
        let normalizedSearchedString = normalizeAndStripString(aSearchedString);
        let normalizedNeedleString = normalizeAndStripString(aNeedleString);
        if (normalizedSearchedString.indexOf(normalizedNeedleString) === 0) {
            matchResult.isMatch = true;
        }
        return matchResult;
    }
}
exports.default = LocationNameMatcher;
function normalizeAndStripString(aString) {
    return stripSpacesAndSpecialCharacters(aString.normalize('NFD').toLowerCase());
}
function stripSpacesAndSpecialCharacters(string) {
    let matches = string.match(/[a-zA-Z]+/g);
    if (matches) {
        return matches.join().replace(/,/g, '');
    }
    return '';
}
