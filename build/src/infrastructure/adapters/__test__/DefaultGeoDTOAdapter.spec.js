"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const GeoBuilder_1 = require("../../../__builders__/GeoBuilder");
const DefaultGeoDTOAdapter_1 = require("../DefaultGeoDTOAdapter");
describe('DefaultGeoDTOAdapter', () => {
    const adapter = new DefaultGeoDTOAdapter_1.default();
    it('to essential DTO', () => {
        const geo = GeoBuilder_1.default.aGeo().ofIstatCode('001001').build();
        const expected = {
            city: {
                istatCode: '001001',
                lat: '45.36343304',
                lng: '7.7686',
            },
            province: { id: '001' },
            region: { id: '01' },
        };
        expect(adapter.toEssentialDTO(geo)).toEqual(expected);
    });
    it('to full DTO', () => {
        const geo = GeoBuilder_1.default.aGeo().ofIstatCode('001001').build();
        const expected = {
            city: {
                istatCode: '001001',
                name: 'Agliè',
                provinceCode: 'TO',
                provinceId: '001',
                regionId: '01',
                type: 'CITY',
                lat: '45.36343304',
                lng: '7.7686',
            },
            province: {
                id: '001',
                name: 'Torino',
                provinceCode: 'TO',
                regionId: '01',
                type: 'PROVINCE'
            },
            region: {
                id: '01',
                name: 'Piemonte',
                regionCode: 'PIEMONTE',
                type: 'REGION'
            }
        };
        expect(adapter.toFullDTO(geo)).toEqual(expected);
    });
});
