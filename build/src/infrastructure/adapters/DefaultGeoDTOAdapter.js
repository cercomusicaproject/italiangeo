"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class DefaultGeoDTOAdapter {
    toEssentialDTO(aGeo) {
        return {
            city: {
                istatCode: aGeo.city.istatCode,
                lat: aGeo.city.lat,
                lng: aGeo.city.lng,
            },
            province: { id: aGeo.province.id },
            region: { id: aGeo.region.id },
        };
    }
    toFullDTO(aGeo) {
        return aGeo.toJSON();
    }
}
exports.default = DefaultGeoDTOAdapter;
