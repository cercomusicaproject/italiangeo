import Geo from '../../domain/Geo';
import { FullGeoDTO } from '../../domain/dtos/FullGeoDTOs';
import GeoDTOAdapter from '../../domain/adapters/GeoDTOAdapter';
import { EssentialGeoDTO } from '../../domain/dtos/EssentialGeoDTOs';
export default class DefaultGeoDTOAdapter implements GeoDTOAdapter {
    toEssentialDTO(aGeo: Geo): EssentialGeoDTO;
    toFullDTO(aGeo: Geo): FullGeoDTO;
}
