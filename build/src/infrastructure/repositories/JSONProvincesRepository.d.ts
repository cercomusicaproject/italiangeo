import Province from '../../domain/Province';
import LocationNameMatcher from '../matchers/LocationNameMatcher';
import ProvincesRepository from '../../domain/repositories/ProvincesRepository';
export default class JSONProvincesRepository implements ProvincesRepository {
    private readonly locationNameMatcher;
    constructor(aLocationNameMatcher: LocationNameMatcher);
    findById(aId: string): Province;
    findAllByRegionId(aRegionId: string): Array<Province>;
    findAllByMatchingName(aNameNeedle: string): Array<Province>;
}
