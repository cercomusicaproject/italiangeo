"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const regionsJSON = require("../../../data/IT/regions.json");
const Region_1 = require("../../domain/Region");
const RegionNotFoundException_1 = require("../../domain/exceptions/RegionNotFoundException");
class JSONRegionsRepository {
    constructor(aLocationNameMatcher) {
        this.locationNameMatcher = aLocationNameMatcher;
    }
    findById(aId) {
        let regionJSON = regionsJSON.find(filterRegionJSONById
            .bind(null, aId));
        if (!doesRegionExist(regionJSON)) {
            throw new RegionNotFoundException_1.default(`No region with id "${aId}" was found`);
        }
        return adaptRegionJSONToDomain(regionJSON);
    }
    findAllByMatchingName(aNameNeedle) {
        let listOfRegionsWithAMatchingName = regionsJSON
            .filter(aRegionJSON => {
            return this.locationNameMatcher
                .findMatch(aRegionJSON.name, aNameNeedle)
                .isMatch;
        })
            .map(adaptRegionJSONToDomain);
        if (!listOfRegionsWithAMatchingName.length) {
            throw new RegionNotFoundException_1.default(`No region matching name "${aNameNeedle}" was found`);
        }
        return listOfRegionsWithAMatchingName;
    }
    all() {
        return regionsJSON.map(adaptRegionJSONToDomain);
    }
}
exports.default = JSONRegionsRepository;
function filterRegionJSONById(aId, aRegionJSON) {
    return aRegionJSON.id === aId;
}
function doesRegionExist(aRegionJSON) {
    return !!aRegionJSON;
}
function adaptRegionJSONToDomain(aRegionJSON) {
    return new Region_1.default(aRegionJSON);
}
