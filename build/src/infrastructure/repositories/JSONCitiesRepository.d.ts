import City from '../../domain/City';
import LocationNameMatcher from '../matchers/LocationNameMatcher';
import CitiesRepository from '../../domain/repositories/CitiesRepository';
export default class JSONCitiesRepository implements CitiesRepository {
    private readonly locationNameMatcher;
    constructor(aLocationNameMatcher: LocationNameMatcher);
    findByIstatCode(aIstatCode: string): City;
    findAllByProvinceId(aProvinceId: string): Array<City>;
    findAllByMatchingName(aNameNeedle: string): Array<City>;
}
