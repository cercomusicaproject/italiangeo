"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Province_1 = require("../../../domain/Province");
const JSONProvincesRepository_1 = require("../JSONProvincesRepository");
const LocationNameMatcher_1 = require("../../matchers/LocationNameMatcher");
const ProvinceNotFoundException_1 = require("../../../domain/exceptions/ProvinceNotFoundException");
describe('JSONProvincesRepository', () => {
    let repo;
    let aMatcher;
    beforeEach(() => {
        aMatcher = new LocationNameMatcher_1.default();
        repo = new JSONProvincesRepository_1.default(aMatcher);
    });
    describe('when required to find a province by id', () => {
        describe('and the province with that id does exist', () => {
            it('should return the province model', () => {
                let aId = '001';
                let province = repo.findById(aId);
                expect(province).toBeInstanceOf(Province_1.default);
                expect(province.id).toBe(aId);
            });
        });
        describe('and the province with that id does NOT exist', () => {
            it('should throw a ProvinceNotFoundException exception', () => {
                expect(() => { repo.findById('BOH'); })
                    .toThrow(ProvinceNotFoundException_1.default);
            });
        });
    });
    describe('when required to find all provinces in a region', () => {
        describe('and some province with that region id exist', () => {
            it('should return the list of provinces in that region', () => {
                let aRegionId = '01';
                let provinces = repo.findAllByRegionId(aRegionId);
                expect(provinces.length).toBe(8);
                provinces.forEach(province => {
                    expect(province.regionId).toBe('01');
                });
            });
        });
        describe('and NO city with that province code exist', () => {
            it('should throw a CityNotFoundException exception', () => {
                expect(() => { repo.findAllByRegionId('BOH'); })
                    .toThrow(ProvinceNotFoundException_1.default);
            });
        });
    });
    describe('when required to find all provinces matching a partial name', () => {
        describe('and some province matching that name exists', () => {
            it('should return the list of matching provinces', () => {
                let aNameNeedle = 'rom';
                let provinces = repo.findAllByMatchingName(aNameNeedle);
                expect(provinces.length).toBe(1);
                expect(provinces[0].name).toBe('Roma');
            });
        });
        describe('and NO province matching that name exists', () => {
            it('should throw a ProvinceNotFoundException exception', () => {
                expect(() => { repo.findAllByMatchingName('BOH'); })
                    .toThrow(ProvinceNotFoundException_1.default);
            });
        });
    });
});
