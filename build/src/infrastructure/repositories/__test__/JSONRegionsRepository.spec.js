"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Region_1 = require("../../../domain/Region");
const JSONRegionsRepository_1 = require("../JSONRegionsRepository");
const LocationNameMatcher_1 = require("../../matchers/LocationNameMatcher");
const RegionNotFoundException_1 = require("../../../domain/exceptions/RegionNotFoundException");
describe('JSONRegionsRepository', () => {
    let repo;
    let aLocationNameMatcher;
    beforeEach(() => {
        aLocationNameMatcher = new LocationNameMatcher_1.default();
        repo = new JSONRegionsRepository_1.default(aLocationNameMatcher);
    });
    describe('when required to find a region by id', () => {
        describe('and the region with that id does exist', () => {
            it('should return the region model', () => {
                let aId = '01';
                let region = repo.findById(aId);
                expect(region).toBeInstanceOf(Region_1.default);
                expect(region.id).toBe(aId);
            });
        });
        describe('and the region with that id does NOT exist', () => {
            it('should throw a RegionNotFoundException exception', () => {
                expect(() => { repo.findById('BOH'); })
                    .toThrow(RegionNotFoundException_1.default);
            });
        });
    });
    describe('when required to find all regions matching a partial name', () => {
        describe('and some region matching that name exists', () => {
            it('should return the list of matching regions', () => {
                let aNameNeedle = 'emi';
                let regions = repo.findAllByMatchingName(aNameNeedle);
                expect(regions.length).toBe(1);
                expect(regions[0].name).toBe('Emilia-Romagna');
            });
        });
        describe('and NO region matching that name exists', () => {
            it('should throw a RegionNotFoundException exception', () => {
                expect(() => { repo.findAllByMatchingName('BOH'); })
                    .toThrow(RegionNotFoundException_1.default);
            });
        });
    });
    it('lists all regions', () => {
        const allRegions = repo.all();
        expect(allRegions.length).toEqual(20);
        allRegions.forEach(it => expect(it).toBeInstanceOf(Region_1.default));
    });
});
