"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const City_1 = require("../../../domain/City");
const JSONCitiesRepository_1 = require("../JSONCitiesRepository");
const LocationNameMatcher_1 = require("../../matchers/LocationNameMatcher");
const CityNotFoundException_1 = require("../../../domain/exceptions/CityNotFoundException");
describe('JSONCitiesRepository', () => {
    let repo;
    let aMatcher;
    beforeEach(() => {
        aMatcher = new LocationNameMatcher_1.default();
        repo = new JSONCitiesRepository_1.default(aMatcher);
    });
    describe('when required to find a city by istatCode', () => {
        describe('and the city with that istatCode does exist', () => {
            it('should return the city model', () => {
                let aIstatCode = '001001';
                let city = repo.findByIstatCode(aIstatCode);
                expect(city).toBeInstanceOf(City_1.default);
                expect(city.istatCode).toBe(aIstatCode);
            });
        });
        describe('and the city with that istatCode does NOT exist', () => {
            it('should throw a CityNotFoundException exception', () => {
                expect(() => { repo.findByIstatCode('BOH'); })
                    .toThrow(CityNotFoundException_1.default);
            });
        });
    });
    describe('when required to find all cities in a province', () => {
        describe('and some city with that province code exist', () => {
            it('should return the list of cities in that province', () => {
                let aProvinceId = '001';
                let cities = repo.findAllByProvinceId(aProvinceId);
                expect(cities.length).toBe(316);
                cities.forEach(city => {
                    expect(city.provinceId).toBe('001');
                });
            });
        });
        describe('and NO city with that province code exist', () => {
            it('should throw a CityNotFoundException exception', () => {
                expect(() => { repo.findAllByProvinceId('BOH'); })
                    .toThrow(CityNotFoundException_1.default);
            });
        });
    });
    describe('when required to find all cities matching a partial name', () => {
        describe('and some city matching that name exists', () => {
            it('should return the list of matching cities', () => {
                let aNameNeedle = 'rom';
                let cities = repo.findAllByMatchingName(aNameNeedle);
                expect(cities.length).toBe(15);
                expect(cities[0].name).toBe('Romano Canavese');
                expect(cities[14].name).toBe('Romana');
            });
        });
        describe('and NO city matching that name exists', () => {
            it('should throw a CityNotFoundException exception', () => {
                expect(() => { repo.findAllByMatchingName('BOH'); })
                    .toThrow(CityNotFoundException_1.default);
            });
        });
    });
});
