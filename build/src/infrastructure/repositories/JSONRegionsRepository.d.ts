import Region from '../../domain/Region';
import LocationNameMatcher from '../matchers/LocationNameMatcher';
import RegionsRepository from '../../domain/repositories/RegionsRepository';
export default class JSONRegionsRepository implements RegionsRepository {
    private readonly locationNameMatcher;
    constructor(aLocationNameMatcher: LocationNameMatcher);
    findById(aId: string): Region;
    findAllByMatchingName(aNameNeedle: string): Array<Region>;
    all(): Array<Region>;
}
