"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const provincesJSON = require("../../../data/IT/provinces.json");
const Province_1 = require("../../domain/Province");
const ProvinceNotFoundException_1 = require("../../domain/exceptions/ProvinceNotFoundException");
class JSONProvincesRepository {
    constructor(aLocationNameMatcher) {
        this.locationNameMatcher = aLocationNameMatcher;
    }
    findById(aId) {
        let provinceJSON = provincesJSON.find(filterProvinceJSONById
            .bind(null, aId));
        if (!doesProvinceExist(provinceJSON)) {
            throw new ProvinceNotFoundException_1.default(`No province with istat code "${aId}" was found`);
        }
        return adaptProvinceJSONToDomain(provinceJSON);
    }
    findAllByRegionId(aRegionId) {
        let listOfProvincesForRegionId = provincesJSON
            .filter(filterProvinceJSONByRegionId
            .bind(null, aRegionId))
            .map(adaptProvinceJSONToDomain);
        if (!listOfProvincesForRegionId.length) {
            throw new ProvinceNotFoundException_1.default(`No province for region id "${aRegionId}" was found`);
        }
        return listOfProvincesForRegionId;
    }
    findAllByMatchingName(aNameNeedle) {
        let listOfProvincesWithAMatchingName = provincesJSON
            .filter(aProvinceJSON => {
            return this.locationNameMatcher
                .findMatch(aProvinceJSON.name, aNameNeedle)
                .isMatch;
        })
            .map(adaptProvinceJSONToDomain);
        if (!listOfProvincesWithAMatchingName.length) {
            throw new ProvinceNotFoundException_1.default(`No province matching name "${aNameNeedle}" was found`);
        }
        return listOfProvincesWithAMatchingName;
    }
}
exports.default = JSONProvincesRepository;
function filterProvinceJSONById(aId, aProvinceJSON) {
    return aProvinceJSON.id === aId;
}
function doesProvinceExist(aProvinceJSON) {
    return !!aProvinceJSON;
}
function adaptProvinceJSONToDomain(aProvinceJSON) {
    return new Province_1.default(aProvinceJSON);
}
function filterProvinceJSONByRegionId(aRegionId, aProvinceJSON) {
    return aProvinceJSON.regionId === aRegionId;
}
