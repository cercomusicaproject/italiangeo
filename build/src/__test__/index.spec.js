"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const Geo_1 = require("../domain/Geo");
const City_1 = require("../domain/City");
const Region_1 = require("../domain/Region");
const index_1 = require("../index");
const Province_1 = require("../domain/Province");
describe('The exposed library', () => {
    describe('#cityByIstatCode', () => {
        it('should delegate to the controller and work as expected', () => {
            const city = index_1.geoService.cityByIstatCode('001001');
            expect(city.get().istatCode).toBe('001001');
        });
    });
    describe('#provinceById', () => {
        it('should delegate to the controller and work as expected', () => {
            const province = index_1.geoService.provinceById('001');
            expect(province.get().id).toBe('001');
        });
    });
    describe('#regionById', () => {
        it('should delegate to the controller and work as expected', () => {
            const region = index_1.geoService.regionById('01');
            expect(region.get().id).toBe('01');
        });
    });
    describe('#locationsByName', () => {
        it('should delegate to the controller and work as expected', () => {
            const locations = index_1.geoService.locationsByName('Piemonte');
            expect(locations.length).toBe(1);
        });
    });
    describe('#geoByIstatCode', () => {
        it('should return the expected geo', () => __awaiter(this, void 0, void 0, function* () {
            const geo = yield index_1.geoService.geoByIstatCode('001001');
            const expected = new Geo_1.default({
                city: new City_1.default({
                    istatCode: '001001',
                    name: 'Agliè',
                    provinceCode: 'TO',
                    provinceId: '001',
                    regionId: '01',
                    lat: '45.36343304',
                    lng: '7.7686',
                }),
                province: new Province_1.default({
                    id: '001',
                    name: 'Torino',
                    provinceCode: 'TO',
                    regionId: '01',
                }),
                region: new Region_1.default({
                    id: '01',
                    name: 'Piemonte',
                    regionCode: 'PIEMONTE',
                })
            });
            expect(geo).toEqual(expected);
        }));
    });
    it('all regions', () => {
        expect(index_1.geoService.allRegions().length).toEqual(20);
    });
    it('all provinces by region id', () => {
        expect(index_1.geoService.allProvincesByRegion('01').length).toEqual(8);
        expect(index_1.geoService.allProvincesByRegion('02').length).toEqual(1);
    });
});
