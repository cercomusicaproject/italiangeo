"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const functionalprogramming_1 = require("functionalprogramming");
const FindLocationsByIdController_1 = require("../FindLocationsByIdController");
const LocationNameMatcher_1 = require("../../infrastructure/matchers/LocationNameMatcher");
const JSONCitiesRepository_1 = require("../../infrastructure/repositories/JSONCitiesRepository");
const JSONRegionsRepository_1 = require("../../infrastructure/repositories/JSONRegionsRepository");
const JSONProvincesRepository_1 = require("../../infrastructure/repositories/JSONProvincesRepository");
describe('Find locations by id controller', () => {
    let controller;
    let aLocationNameMatcher;
    let aCitiesJSONRepo;
    let aProvincesJSONRepo;
    let aRegionsJSONRepo;
    beforeEach(() => {
        aLocationNameMatcher = new LocationNameMatcher_1.default();
        aCitiesJSONRepo = new JSONCitiesRepository_1.default(aLocationNameMatcher);
        aProvincesJSONRepo = new JSONProvincesRepository_1.default(aLocationNameMatcher);
        aRegionsJSONRepo = new JSONRegionsRepository_1.default(aLocationNameMatcher);
        controller = new FindLocationsByIdController_1.default(aCitiesJSONRepo, aProvincesJSONRepo, aRegionsJSONRepo);
    });
    describe('when required to find a city by IstatCode', () => {
        it('city found', () => {
            expect(controller
                .findCityByIstatCode('001001')
                .valueOf())
                .toEqual(functionalprogramming_1.Optional.some({
                'istatCode': '001001',
                'name': 'Agliè',
                'provinceId': '001',
                'provinceCode': 'TO',
                'regionId': '01',
                'type': 'CITY',
                'lat': '45.36343304',
                'lng': '7.7686',
            }));
        });
        it('city not found', () => {
            expect(controller.findCityByIstatCode('BOH'))
                .toEqual(functionalprogramming_1.Optional.none());
        });
    });
    describe('when required to find a province by id', () => {
        it('province found', () => {
            expect(controller
                .findProvinceById('001')
                .valueOf())
                .toEqual(functionalprogramming_1.Optional.some({
                'id': '001',
                'name': 'Torino',
                'provinceCode': 'TO',
                'regionId': '01',
                'type': 'PROVINCE',
            }));
        });
        it('province not found', () => {
            expect(controller.findProvinceById('BOH'))
                .toEqual(functionalprogramming_1.Optional.none());
        });
    });
    describe('when required to find a region by id', () => {
        it('region found', () => {
            expect(controller
                .findRegionById('01')
                .valueOf())
                .toEqual(functionalprogramming_1.Optional.some({
                'id': '01',
                'name': 'Piemonte',
                'regionCode': 'PIEMONTE',
                'type': 'REGION',
            }));
        });
        it('region not found', () => {
            expect(controller.findRegionById('BOH'))
                .toEqual(functionalprogramming_1.Optional.none());
        });
    });
});
