"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const City_1 = require("../../domain/City");
const Region_1 = require("../../domain/Region");
const Province_1 = require("../../domain/Province");
const FindLocationsByNameController_1 = require("../FindLocationsByNameController");
const LocationNameMatcher_1 = require("../../infrastructure/matchers/LocationNameMatcher");
const JSONCitiesRepository_1 = require("../../infrastructure/repositories/JSONCitiesRepository");
const JSONRegionsRepository_1 = require("../../infrastructure/repositories/JSONRegionsRepository");
const JSONProvincesRepository_1 = require("../../infrastructure/repositories/JSONProvincesRepository");
describe('Find locations by name controller', () => {
    let controller;
    let aLocationNameMatcher;
    let aCitiesJSONRepo;
    let aProvincesJSONRepo;
    let aRegionsJSONRepo;
    beforeEach(() => {
        aLocationNameMatcher = new LocationNameMatcher_1.default();
        aCitiesJSONRepo = new JSONCitiesRepository_1.default(aLocationNameMatcher);
        aProvincesJSONRepo = new JSONProvincesRepository_1.default(aLocationNameMatcher);
        aRegionsJSONRepo = new JSONRegionsRepository_1.default(aLocationNameMatcher);
        controller = new FindLocationsByNameController_1.default(aCitiesJSONRepo, aProvincesJSONRepo, aRegionsJSONRepo);
    });
    describe('when required to find a location by matching name', () => {
        describe('and no location types list is provided', () => {
            it('should return matching cities, provinces and regions, if any', () => {
                let matchingLocations = controller
                    .findByMatchingName('lo');
                expect(matchingLocations.length).toBe(61);
                expect(matchingLocations[0]).toBeInstanceOf(City_1.default);
                expect(matchingLocations[59]).toBeInstanceOf(Province_1.default);
                expect(matchingLocations[60]).toBeInstanceOf(Region_1.default);
            });
        });
        describe('and a location types list is provided', () => {
            describe('with only cities allowed', () => {
                it('should return matching cities, if any', () => {
                    let matchingLocations = controller
                        .findByMatchingName('emilia', ['CITIES']);
                    matchingLocations.forEach(aMatchingLocation => {
                        expect(aMatchingLocation).toBeInstanceOf(City_1.default);
                    });
                });
            });
            describe('with only provinces allowed', () => {
                it('should return matching provinces, if any', () => {
                    let matchingLocations = controller
                        .findByMatchingName('emilia', ['PROVINCES']);
                    matchingLocations.forEach(aMatchingLocation => {
                        expect(aMatchingLocation).toBeInstanceOf(Province_1.default);
                    });
                });
            });
            describe('with only regions allowed', () => {
                it('should return matching regions, if any', () => {
                    let matchingLocations = controller
                        .findByMatchingName('emilia', ['REGIONS']);
                    matchingLocations.forEach(aMatchingLocation => {
                        expect(aMatchingLocation).toBeInstanceOf(Region_1.default);
                    });
                });
            });
        });
    });
});
