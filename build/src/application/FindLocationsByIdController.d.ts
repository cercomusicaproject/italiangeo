import { Optional } from 'functionalprogramming';
import City from '../domain/City';
import Region from '../domain/Region';
import Province from '../domain/Province';
import JSONCitiesRepository from '../infrastructure/repositories/JSONCitiesRepository';
import JSONRegionsRepository from '../infrastructure/repositories/JSONRegionsRepository';
import JSONProvincesRepository from '../infrastructure/repositories/JSONProvincesRepository';
export default class FindLocationsByIdController {
    private readonly citiesJSONRepo;
    private readonly provincesJSONRepo;
    private readonly regionsJSONRepo;
    constructor(citiesJSONRepo: JSONCitiesRepository, provincesJSONRepo: JSONProvincesRepository, regionsJSONRepo: JSONRegionsRepository);
    findCityByIstatCode(aIstatCode: string): Optional<City>;
    findProvinceById(aProvinceId: string): Optional<Province>;
    findRegionById(aRegionId: string): Optional<Region>;
}
