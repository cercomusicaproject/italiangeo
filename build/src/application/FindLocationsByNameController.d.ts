import City from '../domain/City';
import Region from '../domain/Region';
import Province from '../domain/Province';
import JSONCitiesRepository from '../infrastructure/repositories/JSONCitiesRepository';
import JSONRegionsRepository from '../infrastructure/repositories/JSONRegionsRepository';
import JSONProvincesRepository from '../infrastructure/repositories/JSONProvincesRepository';
export default class FindLocationsByNameController {
    private readonly citiesJSONRepo;
    private readonly provincesJSONRepo;
    private readonly regionsJSONRepo;
    constructor(citiesJSONRepo: JSONCitiesRepository, provincesJSONRepo: JSONProvincesRepository, regionsJSONRepo: JSONRegionsRepository);
    findByMatchingName(aNameNeedle: string, aListOfLocationTypes?: Array<string>): Array<City | Region | Province>;
    findCitiesByMatchingName(aNameNeedle: string): Array<City>;
    findProvincesByMatchingName(aNameNeedle: string): Array<Province>;
    findRegionsByMatchingName(aNameNeedle: string): Array<Region>;
}
