"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class ListGeoController {
    constructor(provincesRepository, regionsRepository) {
        this.provincesRepository = provincesRepository;
        this.regionsRepository = regionsRepository;
    }
    allRegions() {
        return this.regionsRepository.all();
    }
    allProvincesByRegion(regionId) {
        return this.provincesRepository.findAllByRegionId(regionId);
    }
}
exports.ListGeoController = ListGeoController;
