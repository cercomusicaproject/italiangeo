import Region from '../domain/Region';
import Province from '../domain/Province';
import RegionsRepository from '../domain/repositories/RegionsRepository';
import ProvincesRepository from '../domain/repositories/ProvincesRepository';
export declare class ListGeoController {
    private readonly provincesRepository;
    private readonly regionsRepository;
    constructor(provincesRepository: ProvincesRepository, regionsRepository: RegionsRepository);
    allRegions(): Array<Region>;
    allProvincesByRegion(regionId: string): Array<Province>;
}
