"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const functionalprogramming_1 = require("functionalprogramming");
class FindLocationsByIdController {
    constructor(citiesJSONRepo, provincesJSONRepo, regionsJSONRepo) {
        this.citiesJSONRepo = citiesJSONRepo;
        this.provincesJSONRepo = provincesJSONRepo;
        this.regionsJSONRepo = regionsJSONRepo;
    }
    findCityByIstatCode(aIstatCode) {
        return executeOrEmpty(() => this.citiesJSONRepo.findByIstatCode(aIstatCode));
    }
    findProvinceById(aProvinceId) {
        return executeOrEmpty(() => this.provincesJSONRepo.findById(aProvinceId));
    }
    findRegionById(aRegionId) {
        return executeOrEmpty(() => this.regionsJSONRepo.findById(aRegionId));
    }
}
exports.default = FindLocationsByIdController;
function executeOrEmpty(aOperation) {
    try {
        return functionalprogramming_1.Optional.some(aOperation());
    }
    catch (aException) {
        return functionalprogramming_1.Optional.none();
    }
}
