"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function onUnexpectedSuccess() {
    throw new Error('Unexpected test success');
}
exports.onUnexpectedSuccess = onUnexpectedSuccess;
