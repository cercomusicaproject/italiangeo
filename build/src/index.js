"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const GeoBuilder_1 = require("./__builders__/GeoBuilder");
exports.GeoBuilder = GeoBuilder_1.default;
const Geo_1 = require("./domain/Geo");
exports.Geo = Geo_1.default;
const City_1 = require("./domain/City");
exports.City = City_1.default;
const Region_1 = require("./domain/Region");
exports.Region = Region_1.default;
const Province_1 = require("./domain/Province");
exports.Province = Province_1.default;
const DefaultGeoResolver_1 = require("./domain/services/DefaultGeoResolver");
const LocationNameMatcher_1 = require("./infrastructure/matchers/LocationNameMatcher");
const DefaultGeoDTOAdapter_1 = require("./infrastructure/adapters/DefaultGeoDTOAdapter");
const FindLocationsByIdController_1 = require("./application/FindLocationsByIdController");
const JSONCitiesRepository_1 = require("./infrastructure/repositories/JSONCitiesRepository");
const JSONRegionsRepository_1 = require("./infrastructure/repositories/JSONRegionsRepository");
const FindLocationsByNameController_1 = require("./application/FindLocationsByNameController");
const JSONProvincesRepository_1 = require("./infrastructure/repositories/JSONProvincesRepository");
const ListGeoController_1 = require("./application/ListGeoController");
const locationNameMatcher = new LocationNameMatcher_1.default();
const aJSONCitiesRepository = new JSONCitiesRepository_1.default(locationNameMatcher);
const aJSONProvincesRepository = new JSONProvincesRepository_1.default(locationNameMatcher);
const aJSONRegionsRepository = new JSONRegionsRepository_1.default(locationNameMatcher);
const findLocationsByIdController = new FindLocationsByIdController_1.default(aJSONCitiesRepository, aJSONProvincesRepository, aJSONRegionsRepository);
const findLocationsByNameController = new FindLocationsByNameController_1.default(aJSONCitiesRepository, aJSONProvincesRepository, aJSONRegionsRepository);
const geoResolver = new DefaultGeoResolver_1.default(aJSONCitiesRepository, aJSONProvincesRepository, aJSONRegionsRepository);
const listGeoController = new ListGeoController_1.ListGeoController(aJSONProvincesRepository, aJSONRegionsRepository);
const defaultGeoDTOAdapter = new DefaultGeoDTOAdapter_1.default();
const geoDtoAdapter = defaultGeoDTOAdapter;
exports.geoDtoAdapter = geoDtoAdapter;
const geoService = {
    cityByIstatCode(aIstatCode) {
        return findLocationsByIdController.findCityByIstatCode(aIstatCode);
    },
    provinceById(aProvinceId) {
        return findLocationsByIdController.findProvinceById(aProvinceId);
    },
    regionById(aRegionId) {
        return findLocationsByIdController.findRegionById(aRegionId);
    },
    locationsByName(aNameNeedle, aListOfLocationsTypes) {
        return findLocationsByNameController.findByMatchingName(aNameNeedle, aListOfLocationsTypes);
    },
    geoByIstatCode(aIstatCode) {
        return __awaiter(this, void 0, void 0, function* () {
            return geoResolver.fromIstatCode(aIstatCode);
        });
    },
    allRegions() {
        return listGeoController.allRegions();
    },
    allProvincesByRegion(regionId) {
        return listGeoController.allProvincesByRegion(regionId);
    },
};
exports.geoService = geoService;
