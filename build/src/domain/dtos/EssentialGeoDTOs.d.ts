export declare type EssentialCityDTO = {
    readonly istatCode: string;
    readonly lat: string;
    readonly lng: string;
};
export declare type EssentialProvinceDTO = {
    readonly id: string;
};
export declare type EssentialRegionDTO = {
    readonly id: string;
};
export declare type EssentialGeoDTO = {
    readonly city: EssentialCityDTO;
    readonly province: EssentialProvinceDTO;
    readonly region: EssentialRegionDTO;
};
