export declare type FullCityDTO = {
    readonly type: 'CITY';
    readonly istatCode: string;
    readonly name: string;
    readonly regionId: string;
    readonly provinceId: string;
    readonly provinceCode: string;
    readonly lat: string;
    readonly lng: string;
};
export declare type FullProvinceDTO = {
    readonly type: 'PROVINCE';
    readonly id: string;
    readonly name: string;
    readonly provinceCode: string;
    readonly regionId: string;
};
export declare type FullRegionDTO = {
    readonly type: 'REGION';
    readonly id: string;
    readonly name: string;
    readonly regionCode: string;
};
export declare type FullGeoDTO = {
    readonly city: FullCityDTO;
    readonly province: FullProvinceDTO;
    readonly region: FullRegionDTO;
};
