import Geo from '../Geo';
export default interface GeoResolver {
    fromIstatCode(aCode: string): Promise<Geo>;
}
