import Geo from '../Geo';
import GeoResolver from './GeoResolver';
import CitiesRepository from '../repositories/CitiesRepository';
import RegionsRepository from '../repositories/RegionsRepository';
import ProvincesRepository from '../repositories/ProvincesRepository';
export default class DefaultGeoResolver implements GeoResolver {
    private readonly citiesRepo;
    private readonly provincesRepo;
    private readonly regionsRepo;
    constructor(aCitiesRepo: CitiesRepository, aProvincesRepo: ProvincesRepository, aRegionsRepo: RegionsRepository);
    fromIstatCode(aCode: string): Promise<Geo>;
}
