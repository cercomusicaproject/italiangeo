"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const assertions_1 = require("../../../__testHelpers__/assertions");
const Geo_1 = require("../../Geo");
const City_1 = require("../../City");
const Region_1 = require("../../Region");
const Province_1 = require("../../Province");
const DefaultGeoResolver_1 = require("../DefaultGeoResolver");
const CityNotFoundException_1 = require("../../exceptions/CityNotFoundException");
describe('DefaultGeoResolver', () => {
    let resolver;
    let aCitiesRepo;
    let aRegionsRepo;
    let aProvincesRepo;
    const FOUND_CITY = new City_1.default({
        istatCode: '001001',
        name: 'city',
        regionId: '01',
        provinceId: '001',
        provinceCode: 'PC',
        lat: '0.00',
        lng: '0.00',
    });
    const FOUND_PROVINCE = new Province_1.default({
        id: '001',
        name: 'province',
        provinceCode: 'PC',
        regionId: '01',
    });
    const FOUND_REGION = new Region_1.default({
        id: '01',
        name: 'region',
        regionCode: 'RC',
    });
    beforeEach(() => {
        aCitiesRepo = {
            findByIstatCode() { return FOUND_CITY; },
            findAllByProvinceId() { },
            findAllByMatchingName() { },
        };
        aProvincesRepo = {
            findById() { return FOUND_PROVINCE; },
            findAllByMatchingName() { },
            findAllByRegionId() { },
        };
        aRegionsRepo = {
            findById() { return FOUND_REGION; },
            findAllByMatchingName() { },
            all() { return []; },
        };
        resolver = new DefaultGeoResolver_1.default(aCitiesRepo, aProvincesRepo, aRegionsRepo);
    });
    describe('by ISTAT code', () => {
        it('with existing istat code', () => __awaiter(this, void 0, void 0, function* () {
            jest.spyOn(aCitiesRepo, 'findByIstatCode');
            jest.spyOn(aProvincesRepo, 'findById');
            jest.spyOn(aRegionsRepo, 'findById');
            const existingCode = '001001';
            const geo = yield resolver.fromIstatCode(existingCode);
            const expectedGeo = new Geo_1.default({
                city: FOUND_CITY,
                province: FOUND_PROVINCE,
                region: FOUND_REGION,
            });
            expect(aCitiesRepo.findByIstatCode).toHaveBeenCalledWith('001001');
            expect(aProvincesRepo.findById).toHaveBeenCalledWith('001');
            expect(aRegionsRepo.findById).toHaveBeenCalledWith('01');
            expect(geo).toEqual(expectedGeo);
        }));
        it('with non existing istat code', () => {
            const EXCEPTION = new CityNotFoundException_1.default();
            jest.spyOn(aCitiesRepo, 'findByIstatCode')
                .mockImplementation(() => { throw EXCEPTION; });
            return resolver.fromIstatCode('999999')
                .then(assertions_1.onUnexpectedSuccess)
                .catch(e => expect(e).toBe(EXCEPTION));
        });
    });
});
