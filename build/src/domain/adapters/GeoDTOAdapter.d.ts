import Geo from '../Geo';
import { FullGeoDTO } from '../dtos/FullGeoDTOs';
import { EssentialGeoDTO } from '../dtos/EssentialGeoDTOs';
export default interface GeoDTOAdapter {
    toEssentialDTO(aGeo: Geo): EssentialGeoDTO;
    toFullDTO(aGeo: Geo): FullGeoDTO;
}
