"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Province {
    constructor({ id, name, provinceCode, regionId, }) {
        this.type = 'PROVINCE';
        this.id = id;
        this.name = name;
        this.provinceCode = provinceCode;
        this.regionId = regionId;
    }
    valueOf() {
        return {
            id: this.id,
            name: this.name,
            regionId: this.regionId,
            provinceCode: this.provinceCode,
            type: this.type,
        };
    }
    toJSON() {
        return this.valueOf();
    }
}
exports.default = Province;
