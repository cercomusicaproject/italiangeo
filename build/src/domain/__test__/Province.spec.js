"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const provincesJSON = require("../../../data/IT/provinces.json");
const Province_1 = require("../Province");
describe('Province', () => {
    describe('when initialized', () => {
        it('should expose the province attributes as expected', () => {
            let provinceJSON = provincesJSON[0];
            let province = new Province_1.default(provinceJSON);
            expect(province.id).toBe('001');
            expect(province.name).toBe('Torino');
            expect(province.regionId).toBe('01');
            expect(province.provinceCode).toBe('TO');
            expect(province.type).toBe('PROVINCE');
        });
    });
});
