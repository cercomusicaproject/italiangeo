"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const regionsJSON = require("../../../data/IT/regions.json");
const Region_1 = require("../Region");
describe('Region', () => {
    describe('when initialized', () => {
        it('should expose the region attributes as expected', () => {
            let regionJSON = regionsJSON[0];
            let region = new Region_1.default(regionJSON);
            expect(region.id).toBe('01');
            expect(region.name).toBe('Piemonte');
            expect(region.regionCode).toBe('PIEMONTE');
            expect(region.type).toBe('REGION');
        });
    });
});
