"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Geo {
    constructor({ region, city, province }) {
        this.region = region;
        this.province = province;
        this.city = city;
    }
    valueOf() {
        return {
            city: this.city.valueOf(),
            province: this.province.valueOf(),
            region: this.region.valueOf(),
        };
    }
    toJSON() {
        return this.valueOf();
    }
}
exports.default = Geo;
