import { FullProvinceDTO } from './dtos/FullGeoDTOs';
export default class Province {
    readonly type: 'PROVINCE';
    readonly id: string;
    readonly name: string;
    readonly provinceCode: string;
    readonly regionId: string;
    constructor({ id, name, provinceCode, regionId, }: {
        id: string;
        name: string;
        provinceCode: string;
        regionId: string;
    });
    valueOf(): FullProvinceDTO;
    toJSON(): FullProvinceDTO;
}
