import Province from '../Province';
export default interface ProvincesRepository {
    findById(aId: string): Province;
    findAllByRegionId(aRegionId: string): Array<Province>;
    findAllByMatchingName(aNameNeedle: string): Array<Province>;
}
