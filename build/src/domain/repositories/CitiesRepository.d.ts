import City from '../City';
export default interface CitiesRepository {
    findByIstatCode(aIstatCode: string): City;
    findAllByProvinceId(aProvinceId: string): Array<City>;
    findAllByMatchingName(aNameNeedle: string): Array<City>;
}
