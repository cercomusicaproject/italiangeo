"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Region {
    constructor({ id, name, regionCode, }) {
        this.type = 'REGION';
        this.id = id;
        this.name = name;
        this.regionCode = regionCode;
    }
    valueOf() {
        return {
            id: this.id,
            name: this.name,
            regionCode: this.regionCode,
            type: this.type,
        };
    }
    toJSON() {
        return this.valueOf();
    }
}
exports.default = Region;
