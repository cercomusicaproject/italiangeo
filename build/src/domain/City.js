"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class City {
    constructor({ istatCode, name, regionId, provinceId, provinceCode, lat, lng, }) {
        this.type = 'CITY';
        this.istatCode = istatCode;
        this.name = name;
        this.regionId = regionId;
        this.provinceId = provinceId;
        this.provinceCode = provinceCode;
        this.lat = lat;
        this.lng = lng;
    }
    valueOf() {
        return {
            istatCode: this.istatCode,
            name: this.name,
            regionId: this.regionId,
            provinceId: this.provinceId,
            provinceCode: this.provinceCode,
            type: this.type,
            lat: this.lat,
            lng: this.lng,
        };
    }
    toJSON() {
        return this.valueOf();
    }
}
exports.default = City;
