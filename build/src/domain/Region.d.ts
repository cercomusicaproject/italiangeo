import { FullRegionDTO } from './dtos/FullGeoDTOs';
export default class Region {
    readonly type: 'REGION';
    readonly id: string;
    readonly name: string;
    readonly regionCode: string;
    constructor({ id, name, regionCode, }: {
        id: string;
        name: string;
        regionCode: string;
    });
    valueOf(): FullRegionDTO;
    toJSON(): FullRegionDTO;
}
