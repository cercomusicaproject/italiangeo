import { FullCityDTO } from './dtos/FullGeoDTOs';
export default class City {
    readonly type: 'CITY';
    readonly istatCode: string;
    readonly name: string;
    readonly regionId: string;
    readonly provinceId: string;
    readonly provinceCode: string;
    readonly lat: string;
    readonly lng: string;
    constructor({ istatCode, name, regionId, provinceId, provinceCode, lat, lng, }: {
        istatCode: string;
        name: string;
        regionId: string;
        provinceId: string;
        provinceCode: string;
        lat: string;
        lng: string;
    });
    valueOf(): FullCityDTO;
    toJSON(): FullCityDTO;
}
