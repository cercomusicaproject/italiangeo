"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class CityNotFoundException extends Error {
}
exports.default = CityNotFoundException;
CityNotFoundException.prototype.name = 'CityNotFoundException';
