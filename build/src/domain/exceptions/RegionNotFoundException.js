"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class RegionNotFoundException extends Error {
}
exports.default = RegionNotFoundException;
RegionNotFoundException.prototype.name = 'RegionNotFoundException';
