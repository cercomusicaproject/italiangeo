"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class ProvinceNotFoundException extends Error {
}
exports.default = ProvinceNotFoundException;
ProvinceNotFoundException.prototype.name = 'ProvinceNotFoundException';
