import City from './City';
import Region from './Region';
import Province from './Province';
import { FullGeoDTO } from './dtos/FullGeoDTOs';
export default class Geo {
    readonly region: Region;
    readonly province: Province;
    readonly city: City;
    constructor({ region, city, province }: {
        region: Region;
        city: City;
        province: Province;
    });
    valueOf(): FullGeoDTO;
    toJSON(): FullGeoDTO;
}
