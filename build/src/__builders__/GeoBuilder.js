"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Geo_1 = require("../domain/Geo");
const LocationNameMatcher_1 = require("../infrastructure/matchers/LocationNameMatcher");
const DefaultGeoDTOAdapter_1 = require("../infrastructure/adapters/DefaultGeoDTOAdapter");
const JSONCitiesRepository_1 = require("../infrastructure/repositories/JSONCitiesRepository");
const JSONRegionsRepository_1 = require("../infrastructure/repositories/JSONRegionsRepository");
const JSONProvincesRepository_1 = require("../infrastructure/repositories/JSONProvincesRepository");
class GeoBuilder {
    static aGeo() {
        return new GeoBuilder();
    }
    constructor() {
        const nameMatcher = new LocationNameMatcher_1.default();
        this.dtoAdapter = new DefaultGeoDTOAdapter_1.default();
        this.citiesRepo = new JSONCitiesRepository_1.default(nameMatcher);
        this.provincesRepo = new JSONProvincesRepository_1.default(nameMatcher);
        this.regionsRepo = new JSONRegionsRepository_1.default(nameMatcher);
    }
    ofIstatCode(anIstatCode) {
        this.istatCode = anIstatCode;
        return this;
    }
    build() {
        const city = this.citiesRepo.findByIstatCode(this.istatCode);
        const province = this.provincesRepo.findById(city.provinceId);
        const region = this.regionsRepo.findById(city.regionId);
        return new Geo_1.default({ city, province, region });
    }
    buildFullDTO() {
        return this.dtoAdapter.toFullDTO(this.build());
    }
    buildEssentialDTO() {
        return this.dtoAdapter.toEssentialDTO(this.build());
    }
}
exports.default = GeoBuilder;
