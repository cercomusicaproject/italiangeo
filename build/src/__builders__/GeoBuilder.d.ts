import Geo from '../domain/Geo';
import { FullGeoDTO } from '../domain/dtos/FullGeoDTOs';
import { EssentialGeoDTO } from '../domain/dtos/EssentialGeoDTOs';
export default class GeoBuilder {
    private readonly dtoAdapter;
    private readonly citiesRepo;
    private readonly provincesRepo;
    private readonly regionsRepo;
    private istatCode;
    static aGeo(): GeoBuilder;
    private constructor();
    ofIstatCode(anIstatCode: string): this;
    build(): Geo;
    buildFullDTO(): FullGeoDTO;
    buildEssentialDTO(): EssentialGeoDTO;
}
