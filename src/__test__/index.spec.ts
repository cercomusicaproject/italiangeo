import Geo from '../domain/Geo';
import City from '../domain/City';
import Region from '../domain/Region';
import { geoService } from '../index';
import Province from '../domain/Province';

describe('The exposed library', () => {
  describe('#cityByIstatCode', () => {
    it('should delegate to the controller and work as expected', () => {
      const city = geoService.cityByIstatCode('001001');
      expect(city.get().istatCode).toBe('001001');
    });
  });

  describe('#provinceById', () => {
    it('should delegate to the controller and work as expected', () => {
      const province = geoService.provinceById('001');
      expect(province.get().id).toBe('001');
    });
  });

  describe('#regionById', () => {
    it('should delegate to the controller and work as expected', () => {
      const region = geoService.regionById('01');
      expect(region.get().id).toBe('01');
    });
  });

  describe('#locationsByName', () => {
    it('should delegate to the controller and work as expected', () => {
      const locations = geoService.locationsByName('Piemonte');
      expect(locations.length).toBe(1);
    });
  });

  describe('#geoByIstatCode', () => {
    it('should return the expected geo', async () => {
      const geo = await geoService.geoByIstatCode('001001');

      const expected = new Geo({
        city: new City({
          istatCode: '001001',
          name: 'Agliè',
          provinceCode: 'TO',
          provinceId: '001',
          regionId: '01',
          lat: '45.36343304',
          lng: '7.7686',
        }),
        province: new Province({
          id: '001',
          name: 'Torino',
          provinceCode: 'TO',
          regionId: '01',
        }),
        region: new Region({
          id: '01',
          name: 'Piemonte',
          regionCode: 'PIEMONTE',
        })
      });

      expect(geo).toEqual(expected);
    });
  });

  it('all regions', () => {
    expect(geoService.allRegions().length).toEqual(20);
  });

  it('all provinces by region id', () => {
    expect(geoService.allProvincesByRegion('01').length).toEqual(8);
    expect(geoService.allProvincesByRegion('02').length).toEqual(1);
  });
});
