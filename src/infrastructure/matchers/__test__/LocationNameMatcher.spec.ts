
import LocationNameMatcher from "../LocationNameMatcher";

describe('LocationNameMatcher', () => {
  const matcher = new LocationNameMatcher();

  describe('when required to search a match between two strings without considering special characters', () => {
    describe('and the searched string do start with a match for the needle string', () => {
      describe('with no special characters involved', () => {
        it('should return a "success" match descriptor', () => {
          let aSearchedString = 'ambarabbappero';
          let aNeedleString = 'amba';

          expect(matcher.findMatch(aSearchedString, aNeedleString))
            .toEqual({
              isMatch: true
            });
        });
      });

      describe('with special characters involved', () => {
        it('should return a "success" match descriptor', () => {
          let aSearchedString = 'Ambàrabaá ppero';
          let aNeedleString = 'amba';

          expect(matcher.findMatch(aSearchedString, aNeedleString))
            .toEqual({
              isMatch: true
            });
        });
      });

      describe('with cases involved', () => {
        it('should return a "success" match descriptor', () => {
          let aSearchedString = 'Ambarabaá ppero';
          let aNeedleString = 'ambarabaa';

          expect(matcher.findMatch(aSearchedString, aNeedleString))
            .toEqual({
              isMatch: true
            });
        });
      });

      describe('with unusual strings involved', () => {
        it('should return a "success" match descriptor', () => {
          let aSearchedString = ' `\' ';
          let aNeedleString = '  ';

          expect(matcher.findMatch(aSearchedString, aNeedleString))
            .toEqual({
              isMatch: true
            });
        });
      });
    });

    describe('and the searched string do not start with a match for the needle string', () => {
      it('should return a "no match" descriptor', () => {
        let aSearchedString = 'Ambarabba ppero';
        let aNeedleString = 'arabba';

        expect(matcher.findMatch(aSearchedString, aNeedleString))
          .toEqual({
            isMatch: false
          });
      });

      describe('with unusual strings involved', () => {
        it('should return a "no match" match descriptor', () => {
          let aSearchedString = ' `\' ';
          let aNeedleString = 'a';

          expect(matcher.findMatch(aSearchedString, aNeedleString))
            .toEqual({
              isMatch: false
            });
        });
      });
    });
  });
});
