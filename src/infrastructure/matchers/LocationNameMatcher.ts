
type LocationNameMatchResult = {
  isMatch: boolean
};

export default class LocationNameMatcher {

  findMatch(aSearchedString: string, aNeedleString: string): LocationNameMatchResult  {
    let matchResult: LocationNameMatchResult = {
      isMatch: false
    };

    let normalizedSearchedString = normalizeAndStripString(aSearchedString);
    let normalizedNeedleString = normalizeAndStripString(aNeedleString);

    if (normalizedSearchedString.indexOf(normalizedNeedleString) === 0) {
      matchResult.isMatch = true;
    }

    return matchResult;
  }
}

function normalizeAndStripString(aString) {
  return stripSpacesAndSpecialCharacters(aString.normalize('NFD').toLowerCase());
}

function stripSpacesAndSpecialCharacters(string) {
  let matches = string.match(/[a-zA-Z]+/g);

  if (matches) {
    return matches.join().replace(/,/g, '');
  }

  return '';
}
