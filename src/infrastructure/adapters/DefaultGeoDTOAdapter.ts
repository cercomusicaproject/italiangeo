
import Geo from '../../domain/Geo';
import { FullGeoDTO } from '../../domain/dtos/FullGeoDTOs';
import GeoDTOAdapter from '../../domain/adapters/GeoDTOAdapter';
import { EssentialGeoDTO } from '../../domain/dtos/EssentialGeoDTOs';

export default class DefaultGeoDTOAdapter implements GeoDTOAdapter {

  toEssentialDTO(aGeo: Geo): EssentialGeoDTO {
    return {
      city: {
        istatCode: aGeo.city.istatCode,
        lat: aGeo.city.lat,
        lng: aGeo.city.lng,
      },
      province: { id: aGeo.province.id },
      region: { id: aGeo.region.id },
    };
  }

  toFullDTO(aGeo: Geo): FullGeoDTO {
    return aGeo.toJSON();
  }
}
