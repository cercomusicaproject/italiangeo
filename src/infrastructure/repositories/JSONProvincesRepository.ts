import * as provincesJSON from '../../../data/IT/provinces.json';

import Province from '../../domain/Province';
import LocationNameMatcher from '../matchers/LocationNameMatcher';
import ProvincesRepository from '../../domain/repositories/ProvincesRepository';
import ProvinceNotFoundException from '../../domain/exceptions/ProvinceNotFoundException';

export default class JSONProvincesRepository implements ProvincesRepository {
  private readonly locationNameMatcher: LocationNameMatcher

  constructor(aLocationNameMatcher: LocationNameMatcher) {
    this.locationNameMatcher = aLocationNameMatcher;
  }

  findById(aId: string): Province {
    let provinceJSON = provincesJSON.find(
      filterProvinceJSONById
        .bind(null, aId)
    );

    if (!doesProvinceExist(provinceJSON)) {
      throw new ProvinceNotFoundException(`No province with istat code "${aId}" was found`);
    }

    return adaptProvinceJSONToDomain(provinceJSON);
  }

  findAllByRegionId(aRegionId: string): Array<Province> {
    let listOfProvincesForRegionId = provincesJSON
      .filter(
        filterProvinceJSONByRegionId
          .bind(null, aRegionId)
      )
      .map(adaptProvinceJSONToDomain);

    if (!listOfProvincesForRegionId.length) {
      throw new ProvinceNotFoundException(`No province for region id "${aRegionId}" was found`);
    }

    return listOfProvincesForRegionId;
  }

  findAllByMatchingName(aNameNeedle: string): Array<Province> {
    let listOfProvincesWithAMatchingName = provincesJSON
      .filter(aProvinceJSON => {
        return this.locationNameMatcher
          .findMatch(aProvinceJSON.name, aNameNeedle)
          .isMatch;
      })
      .map(adaptProvinceJSONToDomain);

    if (!listOfProvincesWithAMatchingName.length) {
      throw new ProvinceNotFoundException(`No province matching name "${aNameNeedle}" was found`);
    }

    return listOfProvincesWithAMatchingName;
  }
}

function filterProvinceJSONById(aId, aProvinceJSON) {
  return aProvinceJSON.id === aId;
}

function doesProvinceExist(aProvinceJSON) {
  return !!aProvinceJSON;
}

function adaptProvinceJSONToDomain(aProvinceJSON) {
  return new Province(aProvinceJSON);
}

function filterProvinceJSONByRegionId(aRegionId, aProvinceJSON) {
  return aProvinceJSON.regionId === aRegionId;
}

