import * as regionsJSON from '../../../data/IT/regions.json';

import Region from '../../domain/Region';
import LocationNameMatcher from '../matchers/LocationNameMatcher';
import RegionsRepository from '../../domain/repositories/RegionsRepository';
import RegionNotFoundException from '../../domain/exceptions/RegionNotFoundException';

export default class JSONRegionsRepository implements RegionsRepository {
  private readonly locationNameMatcher: LocationNameMatcher

  constructor(aLocationNameMatcher: LocationNameMatcher) {
    this.locationNameMatcher = aLocationNameMatcher;
  }

  findById(aId: string): Region {
    let regionJSON = regionsJSON.find(
      filterRegionJSONById
        .bind(null, aId)
    );

    if (!doesRegionExist(regionJSON)) {
      throw new RegionNotFoundException(`No region with id "${aId}" was found`);
    }

    return adaptRegionJSONToDomain(regionJSON);
  }

  findAllByMatchingName(aNameNeedle: string): Array<Region> {
    let listOfRegionsWithAMatchingName = regionsJSON
      .filter(aRegionJSON => {
        return this.locationNameMatcher
          .findMatch(aRegionJSON.name, aNameNeedle)
          .isMatch;
      })
      .map(adaptRegionJSONToDomain);

    if (!listOfRegionsWithAMatchingName.length) {
      throw new RegionNotFoundException(`No region matching name "${aNameNeedle}" was found`);
    }

    return listOfRegionsWithAMatchingName;
  }

  all(): Array<Region> {
    return regionsJSON.map(adaptRegionJSONToDomain);
  }
}

function filterRegionJSONById(aId, aRegionJSON) {
  return aRegionJSON.id === aId;
}

function doesRegionExist(aRegionJSON) {
  return !!aRegionJSON;
}

function adaptRegionJSONToDomain(aRegionJSON) {
  return new Region(aRegionJSON);
}
