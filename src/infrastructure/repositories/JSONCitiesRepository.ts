import * as citiesJSON from '../../../data/IT/cities.json';

import City from '../../domain/City';
import LocationNameMatcher from '../matchers/LocationNameMatcher';
import CitiesRepository from '../../domain/repositories/CitiesRepository';
import CityNotFoundException from '../../domain/exceptions/CityNotFoundException';

export default class JSONCitiesRepository implements CitiesRepository {
  private readonly locationNameMatcher: LocationNameMatcher

  constructor(aLocationNameMatcher: LocationNameMatcher) {
    this.locationNameMatcher = aLocationNameMatcher;
  }

  findByIstatCode(aIstatCode: string): City {
    let cityJSON = citiesJSON.find(
      filterCityJSONByIstatCode
        .bind(null, aIstatCode)
    );

    if (!doesCityExist(cityJSON)) {
      throw new CityNotFoundException(`No city with istat code "${aIstatCode}" was found`);
    }

    return adaptCityJSONToDomain(cityJSON);
  }

  findAllByProvinceId(aProvinceId: string): Array<City> {
    let listOfCitiesForProvinceId = citiesJSON
      .filter(
        filterCityJSONByProvinceId
          .bind(null, aProvinceId)
      )
      .map(adaptCityJSONToDomain);

    if (!listOfCitiesForProvinceId.length) {
      throw new CityNotFoundException(`No city for province code "${aProvinceId}" was found`);
    }

    return listOfCitiesForProvinceId;
  }

  findAllByMatchingName(aNameNeedle: string): Array<City> {
    let listOfCitiesWithAMatchingName = citiesJSON
      .filter(aCityJSON => {
        return this.locationNameMatcher
          .findMatch(aCityJSON.name, aNameNeedle)
          .isMatch;
      })
      .map(adaptCityJSONToDomain);

    if (!listOfCitiesWithAMatchingName.length) {
      throw new CityNotFoundException(`No city matching name "${aNameNeedle}" was found`);
    }

    return listOfCitiesWithAMatchingName;
  }
}

function filterCityJSONByIstatCode(aIstatCode, aCityJSON) {
  return aCityJSON.istatCode === aIstatCode;
}

function doesCityExist(aCityJSON) {
  return !!aCityJSON;
}

function adaptCityJSONToDomain(aCityJSON) {
  return new City(aCityJSON);
}

function filterCityJSONByProvinceId(aProvinceId, aCityJSON) {
  return aCityJSON.provinceId === aProvinceId;
}
