import Province from '../../../domain/Province';
import JSONProvincesRepository from '../JSONProvincesRepository';
import LocationNameMatcher from '../../matchers/LocationNameMatcher';
import ProvinceNotFoundException from '../../../domain/exceptions/ProvinceNotFoundException';

describe('JSONProvincesRepository', () => {
  let repo: JSONProvincesRepository;
  let aMatcher: LocationNameMatcher;

  beforeEach(() => {
    aMatcher = new LocationNameMatcher();
    repo = new JSONProvincesRepository(aMatcher);
  });

  describe('when required to find a province by id', () => {
    describe('and the province with that id does exist', () => {
      it('should return the province model', () => {
        let aId = '001';
        let province = repo.findById(aId);

        expect(province).toBeInstanceOf(Province);
        expect(province.id).toBe(aId);
      });
    });

    describe('and the province with that id does NOT exist', () => {
      it('should throw a ProvinceNotFoundException exception', () => {
        expect(() => { repo.findById('BOH'); })
          .toThrow(ProvinceNotFoundException);
      });
    });
  });

  describe('when required to find all provinces in a region', () => {
    describe('and some province with that region id exist', () => {
      it('should return the list of provinces in that region', () => {
        let aRegionId = '01';
        let provinces = repo.findAllByRegionId(aRegionId);

        expect(provinces.length).toBe(8);

        provinces.forEach(province => {
          expect(province.regionId).toBe('01');
        });
      });
    });

    describe('and NO city with that province code exist', () => {
      it('should throw a CityNotFoundException exception', () => {
        expect(() => { repo.findAllByRegionId('BOH'); })
          .toThrow(ProvinceNotFoundException);
      });
    });
  });

  describe('when required to find all provinces matching a partial name', () => {
    describe('and some province matching that name exists', () => {
      it('should return the list of matching provinces', () => {
        let aNameNeedle = 'rom';
        let provinces = repo.findAllByMatchingName(aNameNeedle);

        expect(provinces.length).toBe(1);
        expect(provinces[0].name).toBe('Roma');
      });
    });

    describe('and NO province matching that name exists', () => {
      it('should throw a ProvinceNotFoundException exception', () => {
        expect(() => { repo.findAllByMatchingName('BOH'); })
          .toThrow(ProvinceNotFoundException);
      });
    });
  });
});
