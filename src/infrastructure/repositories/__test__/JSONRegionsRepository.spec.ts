import Region from '../../../domain/Region';
import JSONRegionsRepository from '../JSONRegionsRepository';
import LocationNameMatcher from '../../matchers/LocationNameMatcher';
import RegionNotFoundException from '../../../domain/exceptions/RegionNotFoundException';

describe('JSONRegionsRepository', () => {
  let repo: JSONRegionsRepository;
  let aLocationNameMatcher: LocationNameMatcher;

  beforeEach(() => {
    aLocationNameMatcher = new LocationNameMatcher();
    repo = new JSONRegionsRepository(aLocationNameMatcher);
  });

  describe('when required to find a region by id', () => {
    describe('and the region with that id does exist', () => {
      it('should return the region model', () => {
        let aId = '01';
        let region = repo.findById(aId);

        expect(region).toBeInstanceOf(Region);
        expect(region.id).toBe(aId);
      });
    });

    describe('and the region with that id does NOT exist', () => {
      it('should throw a RegionNotFoundException exception', () => {
        expect(() => { repo.findById('BOH'); })
          .toThrow(RegionNotFoundException);
      });
    });
  });

  describe('when required to find all regions matching a partial name', () => {
    describe('and some region matching that name exists', () => {
      it('should return the list of matching regions', () => {
        let aNameNeedle = 'emi';
        let regions = repo.findAllByMatchingName(aNameNeedle);

        expect(regions.length).toBe(1);
        expect(regions[0].name).toBe('Emilia-Romagna');
      });
    });

    describe('and NO region matching that name exists', () => {
      it('should throw a RegionNotFoundException exception', () => {
        expect(() => { repo.findAllByMatchingName('BOH'); })
          .toThrow(RegionNotFoundException);
      });
    });
  });

  it('lists all regions', () => {
    const allRegions = repo.all();

    expect(allRegions.length).toEqual(20);
    allRegions.forEach(it => expect(it).toBeInstanceOf(Region));
  });
});
