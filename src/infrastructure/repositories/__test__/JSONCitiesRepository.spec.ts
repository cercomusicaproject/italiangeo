import City from '../../../domain/City';
import JSONCitiesRepository from '../JSONCitiesRepository';
import LocationNameMatcher from '../../matchers/LocationNameMatcher';
import CityNotFoundException from '../../../domain/exceptions/CityNotFoundException';

describe('JSONCitiesRepository', () => {
  let repo: JSONCitiesRepository;
  let aMatcher: LocationNameMatcher;

  beforeEach(() => {
    aMatcher = new LocationNameMatcher();
    repo = new JSONCitiesRepository(aMatcher);
  });

  describe('when required to find a city by istatCode', () => {
    describe('and the city with that istatCode does exist', () => {
      it('should return the city model', () => {
        let aIstatCode = '001001';
        let city = repo.findByIstatCode(aIstatCode);

        expect(city).toBeInstanceOf(City);
        expect(city.istatCode).toBe(aIstatCode);
      });
    });

    describe('and the city with that istatCode does NOT exist', () => {
      it('should throw a CityNotFoundException exception', () => {
        expect(() => { repo.findByIstatCode('BOH'); })
          .toThrow(CityNotFoundException);
      });
    });
  });

  describe('when required to find all cities in a province', () => {
    describe('and some city with that province code exist', () => {
      it('should return the list of cities in that province', () => {
        let aProvinceId = '001';
        let cities = repo.findAllByProvinceId(aProvinceId);

        expect(cities.length).toBe(316);

        cities.forEach(city => {
          expect(city.provinceId).toBe('001');
        });
      });
    });

    describe('and NO city with that province code exist', () => {
      it('should throw a CityNotFoundException exception', () => {
        expect(() => { repo.findAllByProvinceId('BOH'); })
          .toThrow(CityNotFoundException);
      });
    });
  });

  describe('when required to find all cities matching a partial name', () => {
    describe('and some city matching that name exists', () => {
      it('should return the list of matching cities', () => {
        let aNameNeedle = 'rom';
        let cities = repo.findAllByMatchingName(aNameNeedle);

        expect(cities.length).toBe(15);
        expect(cities[0].name).toBe('Romano Canavese');
        expect(cities[14].name).toBe('Romana');
      });
    });

    describe('and NO city matching that name exists', () => {
      it('should throw a CityNotFoundException exception', () => {
        expect(() => { repo.findAllByMatchingName('BOH'); })
          .toThrow(CityNotFoundException);
      });
    });
  });
});
