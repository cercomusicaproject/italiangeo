import City from '../domain/City';
import Region from '../domain/Region';
import Province from '../domain/Province';
import JSONCitiesRepository from '../infrastructure/repositories/JSONCitiesRepository';
import JSONRegionsRepository from '../infrastructure/repositories/JSONRegionsRepository';
import JSONProvincesRepository from '../infrastructure/repositories/JSONProvincesRepository';

export default class FindLocationsByNameController {

  constructor(
    private readonly citiesJSONRepo: JSONCitiesRepository,
    private readonly provincesJSONRepo: JSONProvincesRepository,
    private readonly regionsJSONRepo: JSONRegionsRepository,
  ) {}

  findByMatchingName(
    aNameNeedle: string,
    aListOfLocationTypes: Array<string> = null,
  ): Array<City | Region | Province> {
    return []
      .concat(
        findCitiesByMatchingNameIfRequested
          .call(this, aListOfLocationTypes, aNameNeedle),
      )
      .concat(
        findProvincesByMatchingNameIfRequested
          .call(this, aListOfLocationTypes, aNameNeedle),
      )
      .concat(
        findRegionsByMatchingNameIfRequested
          .call(this, aListOfLocationTypes, aNameNeedle),
      );
  }

  findCitiesByMatchingName(aNameNeedle: string): Array<City> {
    return executeOrReturnEmptyOnError(
      this.citiesJSONRepo.findAllByMatchingName
        .bind(this.citiesJSONRepo, aNameNeedle),
    );
  }

  findProvincesByMatchingName(aNameNeedle: string): Array<Province> {
    return executeOrReturnEmptyOnError(
      this.provincesJSONRepo.findAllByMatchingName
        .bind(this.provincesJSONRepo, aNameNeedle),
    );
  }

  findRegionsByMatchingName(aNameNeedle: string): Array<Region> {
    return executeOrReturnEmptyOnError(
      this.regionsJSONRepo.findAllByMatchingName
        .bind(this.regionsJSONRepo, aNameNeedle),
    );
  }
}

function findCitiesByMatchingNameIfRequested(aListOfLocationTypes: Array<string>, aNameNeedle: string): Array<City> {
  return executeIfNeededOrReturnEmpty(
    this.findCitiesByMatchingName.bind(this, aNameNeedle),
    isLocationTypeRequested(aListOfLocationTypes, 'CITIES'),
  );
}

function findProvincesByMatchingNameIfRequested(aListOfLocationTypes: Array<string>, aNameNeedle: string): Array<Province> {
  return executeIfNeededOrReturnEmpty(
    this.findProvincesByMatchingName.bind(this, aNameNeedle),
    isLocationTypeRequested(aListOfLocationTypes, 'PROVINCES'),
  );
}

function findRegionsByMatchingNameIfRequested(aListOfLocationTypes: Array<string>, aNameNeedle: string): Array<Region> {
  return executeIfNeededOrReturnEmpty(
    this.findRegionsByMatchingName.bind(this, aNameNeedle),
    isLocationTypeRequested(aListOfLocationTypes, 'REGIONS'),
  );
}

function isLocationTypeRequested(aListOfLocationTypes, aLocationType) {
  return (
    !aListOfLocationTypes ||
    aListOfLocationTypes.indexOf(aLocationType) !== -1
  );
}

function executeOrReturnEmptyOnError(aOperation) {
  let result = [];

  try {
    result = aOperation();
  } catch (aException) {
  }

  return result;
}

function executeIfNeededOrReturnEmpty(aOperation, aCondition) {
  if (aCondition) {
    return aOperation();
  }

  return [];
}
