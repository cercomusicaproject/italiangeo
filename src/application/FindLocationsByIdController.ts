import { Optional } from 'functionalprogramming';

import City from '../domain/City';
import Region from '../domain/Region';
import Province from '../domain/Province';
import JSONCitiesRepository from '../infrastructure/repositories/JSONCitiesRepository';
import JSONRegionsRepository from '../infrastructure/repositories/JSONRegionsRepository';
import JSONProvincesRepository from '../infrastructure/repositories/JSONProvincesRepository';

export default class FindLocationsByIdController {

  constructor(
    private readonly citiesJSONRepo: JSONCitiesRepository,
    private readonly provincesJSONRepo: JSONProvincesRepository,
    private readonly regionsJSONRepo: JSONRegionsRepository,
  ) {}

  findCityByIstatCode(aIstatCode: string): Optional<City> {
    return executeOrEmpty(() => this.citiesJSONRepo.findByIstatCode(aIstatCode));
  }

  findProvinceById(aProvinceId: string): Optional<Province> {
    return executeOrEmpty(() => this.provincesJSONRepo.findById(aProvinceId));
  }

  findRegionById(aRegionId: string): Optional<Region> {
    return executeOrEmpty(() => this.regionsJSONRepo.findById(aRegionId));
  }
}

function executeOrEmpty<T>(aOperation: () => T): Optional<T> {
  try {
    return Optional.some(aOperation());
  } catch (aException) {
    return Optional.none();
  }
}
