
import City from '../../domain/City';
import Region from '../../domain/Region';
import Province from '../../domain/Province';
import FindLocationsByNameController from '../FindLocationsByNameController';
import LocationNameMatcher from '../../infrastructure/matchers/LocationNameMatcher';
import JSONCitiesRepository from '../../infrastructure/repositories/JSONCitiesRepository';
import JSONRegionsRepository from '../../infrastructure/repositories/JSONRegionsRepository';
import JSONProvincesRepository from '../../infrastructure/repositories/JSONProvincesRepository';

describe('Find locations by name controller', () => {
  let controller: FindLocationsByNameController;
  let aLocationNameMatcher: LocationNameMatcher;
  let aCitiesJSONRepo: JSONCitiesRepository;
  let aProvincesJSONRepo: JSONProvincesRepository;
  let aRegionsJSONRepo: JSONRegionsRepository;

  beforeEach(() => {
    aLocationNameMatcher = new LocationNameMatcher();

    aCitiesJSONRepo = new JSONCitiesRepository(aLocationNameMatcher);
    aProvincesJSONRepo = new JSONProvincesRepository(aLocationNameMatcher);
    aRegionsJSONRepo = new JSONRegionsRepository(aLocationNameMatcher);

    controller = new FindLocationsByNameController(
      aCitiesJSONRepo,
      aProvincesJSONRepo,
      aRegionsJSONRepo
    );
  });

  describe('when required to find a location by matching name', () => {
    describe('and no location types list is provided', () => {
      it('should return matching cities, provinces and regions, if any', () => {
        let matchingLocations = controller
          .findByMatchingName('lo');

        expect(matchingLocations.length).toBe(61);
        expect(matchingLocations[0]).toBeInstanceOf(City);
        expect(matchingLocations[59]).toBeInstanceOf(Province);
        expect(matchingLocations[60]).toBeInstanceOf(Region);
      });
    });

    describe('and a location types list is provided', () => {
      describe('with only cities allowed', () => {
        it('should return matching cities, if any', () => {
          let matchingLocations = controller
            .findByMatchingName('emilia', ['CITIES']);

          matchingLocations.forEach(aMatchingLocation => {
            expect(aMatchingLocation).toBeInstanceOf(City);
          });
        });
      });

      describe('with only provinces allowed', () => {
        it('should return matching provinces, if any', () => {
          let matchingLocations = controller
            .findByMatchingName('emilia', ['PROVINCES']);

          matchingLocations.forEach(aMatchingLocation => {
            expect(aMatchingLocation).toBeInstanceOf(Province);
          });
        });
      });

      describe('with only regions allowed', () => {
        it('should return matching regions, if any', () => {
          let matchingLocations = controller
            .findByMatchingName('emilia', ['REGIONS']);

          matchingLocations.forEach(aMatchingLocation => {
            expect(aMatchingLocation).toBeInstanceOf(Region);
          });
        });
      });
    });
  });
});
