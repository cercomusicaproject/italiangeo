import { Optional } from 'functionalprogramming';

import FindLocationsByIdController from '../FindLocationsByIdController';
import LocationNameMatcher from '../../infrastructure/matchers/LocationNameMatcher';
import JSONCitiesRepository from '../../infrastructure/repositories/JSONCitiesRepository';
import JSONRegionsRepository from '../../infrastructure/repositories/JSONRegionsRepository';
import JSONProvincesRepository from '../../infrastructure/repositories/JSONProvincesRepository';

describe('Find locations by id controller', () => {
  let controller: FindLocationsByIdController;
  let aLocationNameMatcher: LocationNameMatcher;
  let aCitiesJSONRepo: JSONCitiesRepository;
  let aProvincesJSONRepo: JSONProvincesRepository;
  let aRegionsJSONRepo: JSONRegionsRepository;

  beforeEach(() => {
    aLocationNameMatcher = new LocationNameMatcher();
    aCitiesJSONRepo = new JSONCitiesRepository(aLocationNameMatcher);
    aProvincesJSONRepo = new JSONProvincesRepository(aLocationNameMatcher);
    aRegionsJSONRepo = new JSONRegionsRepository(aLocationNameMatcher);

    controller = new FindLocationsByIdController(
      aCitiesJSONRepo,
      aProvincesJSONRepo,
      aRegionsJSONRepo,
    );
  });

  describe('when required to find a city by IstatCode', () => {
    it('city found', () => {
      expect(
        controller
          .findCityByIstatCode('001001')
          .valueOf(),
      )
        .toEqual(Optional.some({
          'istatCode': '001001',
          'name': 'Agliè',
          'provinceId': '001',
          'provinceCode': 'TO',
          'regionId': '01',
          'type': 'CITY',
          'lat': '45.36343304',
          'lng': '7.7686',
        }));
    });

    it('city not found', () => {
      expect(controller.findCityByIstatCode('BOH'))
        .toEqual(Optional.none());
    });
  });

  describe('when required to find a province by id', () => {
    it('province found', () => {
      expect(
        controller
          .findProvinceById('001')
          .valueOf(),
      )
        .toEqual(Optional.some({
          'id': '001',
          'name': 'Torino',
          'provinceCode': 'TO',
          'regionId': '01',
          'type': 'PROVINCE',
        }));
    });

    it('province not found', () => {
      expect(controller.findProvinceById('BOH'))
        .toEqual(Optional.none());
    });
  });

  describe('when required to find a region by id', () => {
    it('region found', () => {
      expect(
        controller
          .findRegionById('01')
          .valueOf(),
      )
        .toEqual(Optional.some({
          'id': '01',
          'name': 'Piemonte',
          'regionCode': 'PIEMONTE',
          'type': 'REGION',
        }));
    });

    it('region not found', () => {
      expect(controller.findRegionById('BOH'))
        .toEqual(Optional.none());
    });
  });
});
