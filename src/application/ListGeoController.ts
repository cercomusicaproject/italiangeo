import Region from '../domain/Region';
import Province from '../domain/Province';
import RegionsRepository from '../domain/repositories/RegionsRepository';
import ProvincesRepository from '../domain/repositories/ProvincesRepository';

export class ListGeoController {

  constructor(
    private readonly provincesRepository: ProvincesRepository,
    private readonly regionsRepository: RegionsRepository,
  ) {}

  allRegions(): Array<Region> {
    return this.regionsRepository.all();
  }

  allProvincesByRegion(regionId: string): Array<Province> {
    return this.provincesRepository.findAllByRegionId(regionId);
  }
}
