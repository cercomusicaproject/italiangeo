
import Geo from '../domain/Geo';
import { FullGeoDTO } from '../domain/dtos/FullGeoDTOs';
import GeoDTOAdapter from '../domain/adapters/GeoDTOAdapter';
import { EssentialGeoDTO } from '../domain/dtos/EssentialGeoDTOs';
import CitiesRepository from '../domain/repositories/CitiesRepository';
import RegionsRepository from '../domain/repositories/RegionsRepository';
import ProvincesRepository from '../domain/repositories/ProvincesRepository';
import LocationNameMatcher from '../infrastructure/matchers/LocationNameMatcher';
import DefaultGeoDTOAdapter from '../infrastructure/adapters/DefaultGeoDTOAdapter';
import JSONCitiesRepository from '../infrastructure/repositories/JSONCitiesRepository';
import JSONRegionsRepository from '../infrastructure/repositories/JSONRegionsRepository';
import JSONProvincesRepository from '../infrastructure/repositories/JSONProvincesRepository';

export default class GeoBuilder {
  private readonly dtoAdapter: GeoDTOAdapter
  private readonly citiesRepo: CitiesRepository
  private readonly provincesRepo: ProvincesRepository
  private readonly regionsRepo: RegionsRepository

  private istatCode: string

  static aGeo(): GeoBuilder {
    return new GeoBuilder();
  }

  private constructor() {
    const nameMatcher = new LocationNameMatcher();
    this.dtoAdapter = new DefaultGeoDTOAdapter();

    this.citiesRepo = new JSONCitiesRepository(nameMatcher);
    this.provincesRepo = new JSONProvincesRepository(nameMatcher);
    this.regionsRepo = new JSONRegionsRepository(nameMatcher);
  }

  ofIstatCode(anIstatCode: string): this {
    this.istatCode = anIstatCode;
    return this;
  }

  build(): Geo {
    const city = this.citiesRepo.findByIstatCode(this.istatCode);
    const province = this.provincesRepo.findById(city.provinceId);
    const region = this.regionsRepo.findById(city.regionId);

    return new Geo({ city, province, region });
  }

  buildFullDTO(): FullGeoDTO {
    return this.dtoAdapter.toFullDTO(this.build());
  }

  buildEssentialDTO(): EssentialGeoDTO {
    return this.dtoAdapter.toEssentialDTO(this.build());
  }
}
