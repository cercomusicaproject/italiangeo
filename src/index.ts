import { Optional } from 'functionalprogramming';

import GeoBuilder from './__builders__/GeoBuilder';

import Geo from './domain/Geo';
import City from './domain/City';
import Region from './domain/Region';
import GeoService from './GeoService';
import Province from './domain/Province';
import GeoDTOAdapter from './domain/adapters/GeoDTOAdapter';
import DefaultGeoResolver from './domain/services/DefaultGeoResolver';
import LocationNameMatcher from './infrastructure/matchers/LocationNameMatcher';
import DefaultGeoDTOAdapter from './infrastructure/adapters/DefaultGeoDTOAdapter';
import FindLocationsByIdController from './application/FindLocationsByIdController';
import JSONCitiesRepository from './infrastructure/repositories/JSONCitiesRepository';
import JSONRegionsRepository from './infrastructure/repositories/JSONRegionsRepository';
import FindLocationsByNameController from './application/FindLocationsByNameController';
import JSONProvincesRepository from './infrastructure/repositories/JSONProvincesRepository';
import { FullCityDTO, FullGeoDTO, FullProvinceDTO, FullRegionDTO } from './domain/dtos/FullGeoDTOs';
import { EssentialCityDTO, EssentialProvinceDTO, EssentialRegionDTO, EssentialGeoDTO } from './domain/dtos/EssentialGeoDTOs';
import { ListGeoController } from './application/ListGeoController';

const locationNameMatcher = new LocationNameMatcher();

const aJSONCitiesRepository = new JSONCitiesRepository(locationNameMatcher);
const aJSONProvincesRepository = new JSONProvincesRepository(locationNameMatcher);
const aJSONRegionsRepository = new JSONRegionsRepository(locationNameMatcher);

const findLocationsByIdController = new FindLocationsByIdController(
  aJSONCitiesRepository,
  aJSONProvincesRepository,
  aJSONRegionsRepository
);

const findLocationsByNameController = new FindLocationsByNameController(
  aJSONCitiesRepository,
  aJSONProvincesRepository,
  aJSONRegionsRepository
);

const geoResolver = new DefaultGeoResolver(
  aJSONCitiesRepository,
  aJSONProvincesRepository,
  aJSONRegionsRepository
);

const listGeoController = new ListGeoController(aJSONProvincesRepository, aJSONRegionsRepository);
const defaultGeoDTOAdapter = new DefaultGeoDTOAdapter();
const geoDtoAdapter = defaultGeoDTOAdapter;

const geoService: GeoService = {

  cityByIstatCode(aIstatCode: string): Optional<City> {
    return findLocationsByIdController.findCityByIstatCode(aIstatCode);
  },

  provinceById(aProvinceId: string): Optional<Province> {
    return findLocationsByIdController.findProvinceById(aProvinceId);
  },

  regionById(aRegionId: string): Optional<Region> {
    return findLocationsByIdController.findRegionById(aRegionId);
  },

  locationsByName(aNameNeedle: string, aListOfLocationsTypes?: Array<string>): Array<City|Province|Region> {
    return findLocationsByNameController.findByMatchingName(aNameNeedle, aListOfLocationsTypes);
  },

  async geoByIstatCode(aIstatCode: string): Promise<Geo> {
    return geoResolver.fromIstatCode(aIstatCode);
  },

  allRegions(): Array<Region> {
    return listGeoController.allRegions();
  },

  allProvincesByRegion(regionId: string): Array<Province> {
    return listGeoController.allProvincesByRegion(regionId);
  },
};

export {

  // Services instances
  geoService,
  geoDtoAdapter,

  // Services interfaces
  GeoService,
  GeoDTOAdapter,

  // Values
  Geo,
  City,
  Region,
  Province,

  // DTOs
  EssentialCityDTO,
  EssentialProvinceDTO,
  EssentialRegionDTO,
  EssentialGeoDTO,
  FullCityDTO,
  FullProvinceDTO,
  FullRegionDTO,
  FullGeoDTO,

  // Test Builders
  GeoBuilder,
};
