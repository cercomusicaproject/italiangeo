
export default class CityNotFoundException extends Error {}
CityNotFoundException.prototype.name = 'CityNotFoundException';
