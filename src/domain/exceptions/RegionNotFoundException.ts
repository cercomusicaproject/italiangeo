
export default class RegionNotFoundException extends Error {}
RegionNotFoundException.prototype.name = 'RegionNotFoundException';
