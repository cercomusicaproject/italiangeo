import City from "../City";
import * as citiesJSON from "../../../data/IT/cities.json";

describe('City', () => {
  it('should expose the city attributes as expected', () => {
    let cityJSON = citiesJSON[0];
    let city = new City(cityJSON as any);

    expect(city.name).toBe('Agliè');
    expect(city.istatCode).toBe('001001');
    expect(city.regionId).toBe('01');
    expect(city.provinceId).toBe('001');
    expect(city.provinceCode).toBe('TO');
    expect(city.lat).toBe('45.36343304');
    expect(city.lng).toBe('7.7686');
    expect(city.type).toBe('CITY');
  });

  it('should serialize as expected', () => {
    let cityJSON = citiesJSON[0];
    let city = new City(cityJSON as any);

    expect(city.toJSON()).toEqual({
      istatCode: '001001',
      name: 'Agliè',
      regionId: '01',
      provinceId: '001',
      provinceCode: 'TO',
      type: 'CITY',
      lat: '45.36343304',
      lng: '7.7686',
    });
  });
});
