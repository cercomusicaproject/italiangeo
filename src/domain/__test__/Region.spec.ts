
import * as regionsJSON from "../../../data/IT/regions.json";
import Region from "../Region";

describe('Region', () => {
  describe('when initialized', () => {
    it('should expose the region attributes as expected', () => {
      let regionJSON = regionsJSON[0];
      let region = new Region(regionJSON);

      expect(region.id).toBe('01');
      expect(region.name).toBe('Piemonte');
      expect(region.regionCode).toBe('PIEMONTE');
      expect(region.type).toBe('REGION');
    });
  });
});
