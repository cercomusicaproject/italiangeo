
import * as provincesJSON from "../../../data/IT/provinces.json";
import Province from "../Province";

describe('Province', () => {
  describe('when initialized', () => {
    it('should expose the province attributes as expected', () => {
      let provinceJSON = provincesJSON[0];
      let province = new Province(provinceJSON);

      expect(province.id).toBe('001');
      expect(province.name).toBe('Torino');
      expect(province.regionId).toBe('01');
      expect(province.provinceCode).toBe('TO');
      expect(province.type).toBe('PROVINCE');
    });
  });
});
