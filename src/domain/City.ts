
import { FullCityDTO } from './dtos/FullGeoDTOs';

export default class City {
  public readonly type: 'CITY';
  public readonly istatCode: string;
  public readonly name: string;
  public readonly regionId: string;
  public readonly provinceId: string;
  public readonly provinceCode: string;
  public readonly lat: string;
  public readonly lng: string;

  constructor({
    istatCode,
    name,
    regionId,
    provinceId,
    provinceCode,
    lat,
    lng,
  }: {
    istatCode: string
    name: string
    regionId: string
    provinceId: string
    provinceCode: string
    lat: string
    lng: string
  }) {
    this.type = 'CITY';

    this.istatCode = istatCode;
    this.name = name;
    this.regionId = regionId;
    this.provinceId = provinceId;
    this.provinceCode = provinceCode;
    this.lat = lat;
    this.lng = lng;
  }

  valueOf(): FullCityDTO {
    return {
      istatCode: this.istatCode,
      name: this.name,
      regionId: this.regionId,
      provinceId: this.provinceId,
      provinceCode: this.provinceCode,
      type: this.type,
      lat: this.lat,
      lng: this.lng,
    }
  }

  toJSON(): FullCityDTO {
    return this.valueOf();
  }
}
