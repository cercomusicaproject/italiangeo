
import Region from '../Region';

export default interface RegionsRepository {
  findById(aId: string): Region
  findAllByMatchingName(aNameNeedle: string): Array<Region>
  all(): Array<Region>;
}
