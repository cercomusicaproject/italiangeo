import { onUnexpectedSuccess } from '../../../__testHelpers__/assertions';

import Geo from '../../Geo';
import City from '../../City';
import Region from '../../Region';
import Province from '../../Province';
import GeoResolver from '../GeoResolver';
import DefaultGeoResolver from '../DefaultGeoResolver';
import CitiesRepository from '../../repositories/CitiesRepository';
import RegionsRepository from '../../repositories/RegionsRepository';
import ProvincesRepository from '../../repositories/ProvincesRepository';
import CityNotFoundException from '../../exceptions/CityNotFoundException';

describe('DefaultGeoResolver', () => {
  let resolver: GeoResolver;
  let aCitiesRepo: CitiesRepository;
  let aRegionsRepo: RegionsRepository;
  let aProvincesRepo: ProvincesRepository;

  const FOUND_CITY = new City({
    istatCode: '001001',
    name: 'city',
    regionId: '01',
    provinceId: '001',
    provinceCode: 'PC',
    lat: '0.00',
    lng: '0.00',
  });

  const FOUND_PROVINCE = new Province({
    id: '001',
    name: 'province',
    provinceCode: 'PC',
    regionId: '01',
  });

  const FOUND_REGION = new Region({
    id: '01',
    name: 'region',
    regionCode: 'RC',
  });

  beforeEach(() => {
    aCitiesRepo = {
      findByIstatCode() { return FOUND_CITY; },
      findAllByProvinceId(): any {},
      findAllByMatchingName(): any {},
    };

    aProvincesRepo = {
      findById() { return FOUND_PROVINCE; },
      findAllByMatchingName(): any {},
      findAllByRegionId(): any {},
    };

    aRegionsRepo = {
      findById() { return FOUND_REGION; },
      findAllByMatchingName(): any {},
      all(): Array<Region> { return []; },
    };

    resolver = new DefaultGeoResolver(aCitiesRepo, aProvincesRepo, aRegionsRepo);
  });

  describe('by ISTAT code', () => {
    it('with existing istat code', async () => {
      jest.spyOn(aCitiesRepo, 'findByIstatCode');
      jest.spyOn(aProvincesRepo, 'findById');
      jest.spyOn(aRegionsRepo, 'findById');

      const existingCode = '001001';
      const geo = await resolver.fromIstatCode(existingCode);

      const expectedGeo = new Geo({
        city: FOUND_CITY,
        province: FOUND_PROVINCE,
        region: FOUND_REGION,
      });

      expect(aCitiesRepo.findByIstatCode).toHaveBeenCalledWith('001001');
      expect(aProvincesRepo.findById).toHaveBeenCalledWith('001');
      expect(aRegionsRepo.findById).toHaveBeenCalledWith('01');

      expect(geo).toEqual(expectedGeo);
    });

    it('with non existing istat code', () => {
      const EXCEPTION = new CityNotFoundException();

      jest.spyOn(aCitiesRepo, 'findByIstatCode')
        .mockImplementation(() => { throw EXCEPTION });

      return resolver.fromIstatCode('999999')
        .then(onUnexpectedSuccess)
        .catch(e => expect(e).toBe(EXCEPTION));
    });
  });
});
