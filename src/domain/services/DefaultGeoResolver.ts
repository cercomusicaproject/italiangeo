
import Geo from '../Geo';
import GeoResolver from './GeoResolver';
import CitiesRepository from '../repositories/CitiesRepository';
import RegionsRepository from '../repositories/RegionsRepository';
import ProvincesRepository from '../repositories/ProvincesRepository';

export default class DefaultGeoResolver implements GeoResolver {
  private readonly citiesRepo: CitiesRepository
  private readonly provincesRepo: ProvincesRepository
  private readonly regionsRepo: RegionsRepository

  constructor(
    aCitiesRepo: CitiesRepository,
    aProvincesRepo: ProvincesRepository,
    aRegionsRepo: RegionsRepository,
  ) {
    this.citiesRepo = aCitiesRepo;
    this.provincesRepo = aProvincesRepo;
    this.regionsRepo = aRegionsRepo;
  }

  async fromIstatCode(aCode: string): Promise<Geo> {
    const city = this.citiesRepo.findByIstatCode(aCode);
    const province = this.provincesRepo.findById(city.provinceId);
    const region = this.regionsRepo.findById(city.regionId);

    return new Geo({ city, province, region });
  }
}
