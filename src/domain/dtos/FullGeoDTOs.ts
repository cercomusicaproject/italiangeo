
export type FullCityDTO = {
  readonly type: 'CITY'
  readonly istatCode: string
  readonly name: string
  readonly regionId: string
  readonly provinceId: string
  readonly provinceCode: string
  readonly lat: string
  readonly lng: string
};

export type FullProvinceDTO = {
  readonly type: 'PROVINCE'
  readonly id: string
  readonly name: string
  readonly provinceCode: string
  readonly regionId: string
};

export type FullRegionDTO = {
  readonly type: 'REGION'
  readonly id: string
  readonly name: string
  readonly regionCode: string
};

export type FullGeoDTO = {
  readonly city: FullCityDTO
  readonly province: FullProvinceDTO
  readonly region: FullRegionDTO
};
