
export type EssentialCityDTO = {
  readonly istatCode: string
  readonly lat: string
  readonly lng: string
};

export type EssentialProvinceDTO = {
  readonly id: string
};

export type EssentialRegionDTO = {
  readonly id: string
};

export type EssentialGeoDTO = {
  readonly city: EssentialCityDTO
  readonly province: EssentialProvinceDTO
  readonly region: EssentialRegionDTO
};
