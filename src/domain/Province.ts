import { FullProvinceDTO } from './dtos/FullGeoDTOs';

export default class Province {
  public readonly type: 'PROVINCE';
  public readonly id: string;
  public readonly name: string;
  public readonly provinceCode: string;
  public readonly regionId: string;

  constructor({
    id,
    name,
    provinceCode,
    regionId,
  }: {
    id: string,
    name: string,
    provinceCode: string,
    regionId: string,
  }) {
    this.type = 'PROVINCE';

    this.id = id;
    this.name = name;
    this.provinceCode = provinceCode;
    this.regionId = regionId;
  }

  valueOf(): FullProvinceDTO {
    return {
      id: this.id,
      name: this.name,
      regionId: this.regionId,
      provinceCode: this.provinceCode,
      type: this.type,
    }
  }

  toJSON(): FullProvinceDTO {
    return this.valueOf();
  }
}
