
import { FullRegionDTO } from './dtos/FullGeoDTOs';

export default class Region {
  public readonly type: 'REGION'
  public readonly id: string
  public readonly name: string
  public readonly regionCode: string

  constructor({
    id,
    name,
    regionCode,
  }: {
    id: string,
    name: string,
    regionCode: string,
  }) {
    this.type = 'REGION';

    this.id = id;
    this.name = name;
    this.regionCode = regionCode;
  }

  valueOf(): FullRegionDTO {
    return {
      id: this.id,
      name: this.name,
      regionCode: this.regionCode,
      type: this.type,
    }
  }

  toJSON(): FullRegionDTO {
    return this.valueOf();
  }
}
