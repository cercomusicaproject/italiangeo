
import City from './City';
import Region from './Region';
import Province from './Province';
import { FullGeoDTO } from './dtos/FullGeoDTOs';

export default class Geo {
  public readonly region: Region
  public readonly province: Province
  public readonly city: City

  constructor(
    { region, city, province }:
    { region: Region, city: City, province: Province }
  ) {
    this.region = region;
    this.province = province;
    this.city = city;
  }

  valueOf(): FullGeoDTO {
    return {
      city: this.city.valueOf(),
      province: this.province.valueOf(),
      region: this.region.valueOf(),
    }
  }

  toJSON(): FullGeoDTO {
    return this.valueOf();
  }
}
