import { Optional } from 'functionalprogramming';

import Geo from './domain/Geo';
import City from './domain/City';
import Region from './domain/Region';
import Province from './domain/Province';

export default interface GeoService {
  cityByIstatCode(aIstatCode: string): Optional<City>;
  provinceById(aProvinceId: string): Optional<Province>;
  regionById(aRegionId: string): Optional<Region>;
  geoByIstatCode(anIstatCode: string): Promise<Geo>;
  locationsByName(aNameNeedle: string, aListOfLocationsTypes?: Array<string>): Array<City|Province|Region>;
  allRegions(): Array<Region>;
  allProvincesByRegion(regionId: string): Array<Province>;
}
