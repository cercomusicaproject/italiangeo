
const fs = require('fs');
const parse = require('csv-parse/lib/sync');
const geo = require('../data/IT/cities_geo');

fs.readFile(`${ __dirname }/../data/IT/cities.csv`, 'utf8', (err, data) => {
  let parsedITACities = parse(data, { columns: true, delimiter: ';' });
  let adaptedITACities = adaptCSVDataToITACities(parsedITACities);

  writeCitiesJSONToDisk(adaptedITACities);
});

function adaptCSVDataToITACities(aListOfParsedCities) {
  return aListOfParsedCities.map(adaptParsedCity);
}

function adaptParsedCity(aParsedCity) {
  const istatCode = aParsedCity['Codice Comune formato alfanumerico'];

  let currentGeoIndex;

  console.log(`Geo length "${ geo.length }"`);

  const coordinates = geo.find((aGeo, index) => {
    const adaptedIstatCode = istatCode.replace(/^0+/, '');

    const found = aGeo.istat === adaptedIstatCode;
    currentGeoIndex = found ? index : null;

    return found;
  });

  if (!coordinates) {
    console.log(`>>>>>> Cannot match latitude and longitude for istat "${ istatCode }"`);
  }

  if (currentGeoIndex) {
    geo.splice(currentGeoIndex, 1);
  }

  return {
    istatCode: istatCode,
    name: coordinates.comune,
    provinceId: aParsedCity['Codice Provincia (1)'],
    provinceCode: aParsedCity['Sigla automobilistica'],
    regionId: aParsedCity['Codice Regione'],
    isProvinceCapital: aParsedCity['Flag Comune capoluogo di provincia'] === '0' ? false : true,
    lat: coordinates ? coordinates.lat : null,
    lng: coordinates ? coordinates.lng : null,
  };
}

function writeCitiesJSONToDisk(aListOfCitiesJSONs) {
  fs.writeFileSync(
    `${ __dirname }/../data/IT/cities.json`,
    JSON.stringify(aListOfCitiesJSONs),
    'utf8'
  );
}

fs.readFile(`${ __dirname }/../data/IT/provinces.csv`, 'latin1', (err, data) => {
  adaptProvincesCSV(data);
  adaptRegionsCSV(data);
});

function adaptProvincesCSV(data) {
  let parsedProvinces = parse(data, { columns: true, delimiter: ';' });
  let adaptedProvinces = adaptCSVDataToProvinces(parsedProvinces);

  writeProvincesJSONToDisk(adaptedProvinces);
}

function adaptCSVDataToProvinces(aListOfParsedProvinces) {
  return aListOfParsedProvinces
    .map(adaptParsedProvince);
}

function adaptParsedProvince(aParsedProvince) {
  return {
    name: adaptProvinceName(aParsedProvince),
    id: aParsedProvince['Codice provincia'],
    provinceCode: aParsedProvince['Sigla automobilistica'],
    regionId: aParsedProvince['Codice regione']
  };
}

function adaptProvinceName(aParsedProvince) {
  let provinceDenomination = aParsedProvince['Denominazione provincia'];
  let metropolitanCityDenomination = aParsedProvince['Denominazione                  Città metropolitana'];

  if (provinceDenomination && provinceDenomination !== '-') {
    return provinceDenomination;
  }

  return metropolitanCityDenomination;
}

function writeProvincesJSONToDisk(aListOfProvincesJSONs) {
  fs.writeFileSync(
    `${ __dirname }/../data/IT/provinces.json`,
    JSON.stringify(aListOfProvincesJSONs),
    'utf8'
  );
}

function adaptRegionsCSV(data) {
  let parsedProvinces = parse(data, { columns: true, delimiter: ';' });
  let adaptedRegions = adaptCSVDataToRegions(parsedProvinces);

  writeRegionsJSONToDisk(adaptedRegions);
}

function adaptCSVDataToRegions(aListOfParsedProvinces) {
  let listOfRegionsIds = [];

  return aListOfParsedProvinces
    .filter(aParsedProvince => {
      let regionId = aParsedProvince['Codice regione'];

      if (regionId && listOfRegionsIds.indexOf(regionId) === -1) {
        listOfRegionsIds.push(regionId);
        return true;
      }

    })
    .map(adaptParsedRegion);
}

function adaptParsedRegion(aParsedRegion) {
  return {
    name: adaptRegionName(aParsedRegion['Denominazione regione']),
    id: aParsedRegion['Codice regione'],
    regionCode: adaptRegionCode(aParsedRegion['Denominazione regione\n(Maiuscolo)'])
  };
}

function adaptRegionCode(aRegionCode) {
  return aRegionCode
    .split('/')[0]
    .replace(/\W+/g, '_');
}

function adaptRegionName(aRegionName) {
  return aRegionName
    .split('/')[0];
}

function writeRegionsJSONToDisk(aListOfRegionsJSONs) {
  fs.writeFileSync(
    `${ __dirname }/../data/IT/regions.json`,
    JSON.stringify(aListOfRegionsJSONs),
    'utf8'
  );
}

